import $ from 'jquery'

const t = localStorage.currentUser
  ? JSON.parse(localStorage.currentUser).token : ''

export default function(url, data, f, e) {
  $.ajax({
    type: 'post',
    url: 'http://localhost:8080/' + url,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
      'Authorization': 'SANGE ' + t
    },
    data: JSON.stringify(data),
    // dataType : "json",
    success: f,
    fail: function(data) {
      console.log(data.status)
    },
    error: e || function(data) {
      if (data.status === 403) {

      }
    }
  })
}
